<?php

namespace App\Http\Controllers;

use App\Models\Keyword;
use Illuminate\Http\Request;

class KeywordController extends Controller {
    
    public function index()     {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function create() {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function store(Request $request) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function show(Keyword $keyword) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function edit(Keyword $keyword) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function update(Request $request, Keyword $keyword) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function destroy(Keyword $keyword) {
        
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function keyword_upload(Request $request) {
        
        $extraction_date = $request->input("extraction_date");
        $source = $request->input("source");

        $csv_string = stripslashes($request->input("csvContent"));
        $csv_lines = explode("\n", $csv_string);

        $csv_entries = array();

        $instance = Keyword::where("extraction_date", $extraction_date)->where("source", $source)->first();

        if($instance) {
            Keyword::where("extraction_date", $extraction_date)->where("source", $source)->delete();
        }

        $keyword_synced = 0;
        foreach($csv_lines as $key => $csv_line) {
            $csv_line_entry = explode('",', $csv_line);
            $csv_line_entry = str_replace("\"","", $csv_line_entry);

            if($key > 0) {

                $to_create_entry = array(
                    "campaign"          => $csv_line_entry[0],
                    "status"            => $csv_line_entry[1],
                    "budget"            => $csv_line_entry[2],
                    "ad_group"          => $csv_line_entry[3],
                    "keyword_label"     => $csv_line_entry[4],
                    "campaign_label"    => $csv_line_entry[5],
                    "keyword_in"        => $csv_line_entry[6],
                    "forced_kwd"        => $csv_line_entry[7],
                    "match_type"        => $csv_line_entry[8],
                    "kwd_delivery"      => $csv_line_entry[9],
                    "bid"               => $csv_line_entry[10],
                    "cost"              => $csv_line_entry[11],
                    "impressions"       => $csv_line_entry[12],
                    "clicks"            => $csv_line_entry[13],
                    "ctr"               => $csv_line_entry[14],
                    "avg_cpc"           => $csv_line_entry[15],
                    "clickout"          => $csv_line_entry[16],
                    "cr"                => $csv_line_entry[17],
                    "rpc"               => $csv_line_entry[18],
                    "revenue"           => $csv_line_entry[19],
                    "gross_margin"      => $csv_line_entry[20],
                    "net_roi"           => $csv_line_entry[21],
                    "cpc_be"            => $csv_line_entry[22],
                    "user"              => $csv_line_entry[23],
                    "country"           => $csv_line_entry[24],

                    "extraction_date"       => $extraction_date,
                    "source"                => $source
                );

                $keyword = Keyword::create($to_create_entry);

                if($keyword) {
                    $keyword_synced++;
                }
            }
        }

        $data = array(
            "keyword_synced" => $keyword_synced,
            "message" => "success"
        );

        return $data;

    }
}
