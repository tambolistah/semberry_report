<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\SplitCampaign;
use Illuminate\Http\Request;

class CampaignController extends Controller {
    
    public function index() {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function create() {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function store(Request $request) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function show(Campaign $campaign) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function edit(Campaign $campaign) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function update(Request $request, Campaign $campaign) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function destroy(Campaign $campaign) {
        //
    }

    /* ------------------------------------------------- CAMPAIGN AD GROUP FUNCTIONS ------------------------------------------------- */

    public function campaign_upload(Request $request) {
        
        $extraction_date = $request->input("extraction_date");
        $source = $request->input("source");

        $csv_string = stripslashes($request->input("csvContent"));
        $csv_lines = explode("\n", $csv_string);

        $csv_entries = array();

        $instance = Campaign::where("extraction_date", $extraction_date)->where("source", $source)->first();

        if($instance) {
            Campaign::where("extraction_date", $extraction_date)->where("source", $source)->delete();
        }

        $campaign_synced = 0;
        foreach($csv_lines as $key => $csv_line) {
            $csv_line_entry = explode('",', $csv_line);
            $csv_line_entry = str_replace("\"","", $csv_line_entry);

            if($key > 0) {

                $to_create_entry = array(
                    "campaign"              => $csv_line_entry[0],
                    "status"                => $csv_line_entry[1],
                    "labels"                => $csv_line_entry[2],
                    "budget"                => $csv_line_entry[3],
                    "bid_strategy_type"     => $csv_line_entry[4],
                    "bid"                   => $csv_line_entry[5],
                    "cost"                  => $csv_line_entry[6],
                    "impressions"           => $csv_line_entry[7],
                    "clicks"                => $csv_line_entry[8],
                    "ctr"                   => $csv_line_entry[9],
                    "avg_cpc"               => $csv_line_entry[10],
                    "clickout"              => $csv_line_entry[11],
                    "cr"                    => $csv_line_entry[12],
                    "rpc"                   => $csv_line_entry[13],
                    "rev"                   => $csv_line_entry[14],
                    "gm"                    => $csv_line_entry[15],
                    "net_roi"               => $csv_line_entry[16],
                    "cpc_be"                => $csv_line_entry[17],
                    "user"                  => $csv_line_entry[18],
                    "country"               => $csv_line_entry[19],
                    "extraction_date"       => $extraction_date,
                    "source"                => $source
                );

                $campaign = Campaign::create($to_create_entry);

                if($campaign) {
                    $campaign_synced++;
                }
            }
        }

        $split_csv_string = stripslashes($request->input("splitCsvContent"));
        $split_csv_lines = explode("\n", $split_csv_string);

        $split_campaign_synced = 0;
        foreach($split_csv_lines as $key => $split_csv_line) {
            $csv_line_entry = explode('",', $split_csv_line);
            $csv_line_entry = str_replace("\"","", $csv_line_entry);

            if($key > 0) {
                $to_create_entry = array(
                    "campaign"              => $csv_line_entry[0],
                    "status"                => $csv_line_entry[1],
                    "labels"                => $csv_line_entry[2],
                    "budget"                => $csv_line_entry[3],
                    
                    "ad_group"              => $csv_line_entry[4],
                    "ad_delivery"           => $csv_line_entry[5],
                    "ad_labels"             => $csv_line_entry[6],

                    "bid_strategy_type"     => $csv_line_entry[7],
                    "bid"                   => $csv_line_entry[8],
                    "cost"                  => $csv_line_entry[9],
                    "impressions"           => $csv_line_entry[10],
                    "clicks"                => $csv_line_entry[11],
                    "ctr"                   => $csv_line_entry[12],
                    "avg_cpc"               => $csv_line_entry[13],
                    "clickout"              => $csv_line_entry[14],
                    "cr"                    => $csv_line_entry[15],
                    "rpc"                   => $csv_line_entry[16],
                    "rev"                   => $csv_line_entry[17],
                    "gm"                    => $csv_line_entry[18],
                    "net_roi"               => $csv_line_entry[19],
                    "cpc_be"                => $csv_line_entry[20],
                    "user"                  => $csv_line_entry[21],
                    "country"               => $csv_line_entry[22],
                    "extraction_date"       => $extraction_date,
                    "source"                => $source
                );

                $split_campaign = SplitCampaign::create($to_create_entry);
                if($split_campaign) {
                    $split_campaign_synced++;
                }
            }

        }

        $data = array(
            "campaign_synced" => $campaign_synced,
            "split_campaign_synced" => $split_campaign_synced,
            "message" => "success"
        );

        return $data;

    }   
}
