<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model {
    use HasFactory;

    protected $table = 'daily_keyword';
    public $fillable = [
      "campaign",
      "status",
      "budget",
      "ad_group",
      "keyword_label",
      "campaign_label",
      "keyword_in",
      "forced_kwd",
      "match_type",
      "kwd_delivery",
      "bid",
      "cost",
      "impressions",
      "clicks",
      "ctr",
      "avg_cpc",
      "clickout",
      "cr",
      "rpc",
      "revenue",
      "gross_margin",
      "net_roi",
      "cpc_be",
      "user",
      "source",
      "extraction_date",
      "country"
    ];
}
