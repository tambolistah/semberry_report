<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SplitCampaign extends Model {
    use HasFactory;

    protected $table = 'daily_campaign_split';
    public $fillable = [
        "campaign",
        "status",
        "labels",
        "budget",
        "ad_group",
        "ad_delivery",
        "ad_labels",
        "bid_strategy_type",
        "bid",
        "cost",
        "impressions",
        "clicks",
        "ctr",
        "avg_cpc",
        "clickout",
        "cr",
        "rpc",
        "rev",
        "gm",
        "net_roi",
        "cpc_be",
        "user",
        "extraction_date",
        "source",
        "country"
    ];
}
