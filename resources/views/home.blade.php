@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard - Realtime</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="campaign-tab" data-toggle="tab" href="#campaign" role="tab" aria-controls="campaign" aria-selected="false">By Campaign</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="keyword-tab" data-toggle="tab" href="#keyword" role="tab" aria-controls="keyword" aria-selected="false">By Keywords</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        </div>
                        <div class="tab-pane fade" id="campaign" role="tabpanel" aria-labelledby="campaign-tab">
                            
                        </div>
                        <div class="tab-pane fade" id="keyword" role="tabpanel" aria-labelledby="keyword-tab">
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready( function () {
            $('#myTable').DataTable();
            $('#aggregatedCampaign').DataTable();
            $('#aggregatedKeyword').DataTable();
        } );
    </script>
@stop